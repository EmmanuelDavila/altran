import { Component, OnInit, ViewChild } from "@angular/core";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
  NavController,
} from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { Averia } from "../modelo/averia";
import { DatosProvider } from "../services/datos";
import { I18nProvider } from "../services/i18n";
import { Services } from "../services/services";
import { HttpClient } from "@angular/common/http";
import { pepito } from "../tabs/tabs.page";

@Component({
  selector: "app-reporte",
  templateUrl: "./reporte.page.html",
  styleUrls: ["./reporte.page.scss"],
})
export class ReportePage implements OnInit {
  variosVehiculos: boolean;
  averiaSeleccionada;
  averias = { breakdowns: [] };
  goToRoot: boolean = true;
  message;
  negative;
  positive;
  titulo;
  public loading: any;
  country_code: any;
  codigo_compania: any;
  personalizacion3: any;
  element: any;
  mushrooms;
  data: [];
  neededArray: any[];
  constructor(
    public datos: DatosProvider,
    public services: Services,
    public toastCtrl: ToastController,
    public platform: Platform,
    public i18n: I18nProvider,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public reporte: Averia,
    public http: HttpClient
  ) {
    //  this.datos.stepView("Reporte");

    this.datos.pantalla = "reporte";

    this.variosVehiculos = false;
    this.getAverias();
  }

  ngOnInit() {
    
  }

  // ionViewWillEnter() {
  //   this.goToRoot = true;
  // }

  // getdar() {
  //   this.datos.setPersonalizacion2().subscribe((res: any) => {
  //     console.log("setPersonalizacion2", res.string);
  //     this.personalizacion3 = res;
  //     console.log("setPersonalizacion3", this.personalizacion3);
  //   });
  // }



  getAverias() {
    console.log("ddddd", pepito);

    this.datos.setPersonalizacion2().subscribe((res: any) => {
      this.services
        .getListOfBreakDownsByCountry(
          res.string.country_code,
          res.string.codigo_compania
        )
        .subscribe(
          (data: any) => {
            this.neededArray = [];
            console.log("neededArray", this.neededArray);

            console.log("entro", data.breakdowns);
            for (let index = 0; index < data.breakdowns.length; index++) {
              this.element = data.breakdowns[index];
              console.log("element", this.element);
              this.neededArray.push(this.element);
              
             let traduccion = this.i18n.translate2(
                this.element.label,
                this.datos.lenguaje_defecto
              )

              console.log('traducc', traduccion);
              
            }

            // }
            // this.loading.dismiss();
          },
          (err) => {
            // this.loading.dismiss()
            this.translate
              .get("alert_conexion_titulo")
              .subscribe((res: string) => {
                this.titulo = res;
              });
            this.translate
              .get("alert_conexion_mensaje")
              .subscribe((res: string) => {
                this.message = res;
              });
            this.presentError(this.message);
            console.log("Error: " + err);
          }
        );
    });
  }
  ionViewWillLeave() {
    // if (this.goToRoot)
    //   this.navCtrl.setRoot(AsistenciaPage);
  }
  anterior() {
    this.goToRoot = false;
    // this.navCtrl.setRoot(DatosPersonalesPage);
  }

  siguiente() {
    if (this.mushrooms) {
      console.log('this.mushrooms',this.mushrooms);
      
      // this.reporte.breakDownType = this.mushrooms;
      // this.goToRoot = false;
      // this.datos.setAveria(this.reporte, null, null, null);
      // this.navCtrl.setRoot(UbicacionPage);
    } else {
      this.translate.get("reporte_selec_averia").subscribe((res: string) => {
        this.message = res;
      });
      this.presentToast(this.message, 2000);
    }
  }

  async presentToast(texto, tiempo) {
    let toast = await this.toastCtrl.create({
      message: texto,
      duration: tiempo,
      position: "bottom",
    });

    toast.present();
  }

  async presentError(message) {
    this.translate.get("confirmacion_aceptar").subscribe((res: string) => {
      this.positive = res;
    });
    let alert = await this.alertCtrl.create({
      message: message,
      buttons: [
        {
          text: this.positive,
          handler: () => {
            this.anterior();
          },
        },
      ],
    });
    alert.present();
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({
      spinner: "crescent",
    });

    this.loading.present();
  }
  goBack(): void {
    this.anterior();
  }
}
