import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportePageRoutingModule } from './reporte-routing.module';

import { ReportePage } from './reporte.page';
import { Http, ConnectionBackend, RequestOptions } from '@angular/http';
import { ConfigProvider } from '../services/config-provider';
import { Services } from '../services/services';
import { I18nProvider } from '../services/i18n';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ReportePageRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  declarations: [ReportePage],
  providers:[
    Services,
    // ConnectionBackend,
    // RequestOptions,
    ConfigProvider,
    // FormBuilder,
     I18nProvider,
     
  ]
})
export class ReportePageModule {}
