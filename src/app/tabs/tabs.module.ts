import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { ConfigProvider } from '../services/config-provider';
import { I18nProvider } from '../services/i18n';
import { DatosProvider } from '../services/datos';
import { Services } from '../services/services';
import { ContactMeansProvider } from '../services/contact-means';
import { ConnectionBackend, Http, RequestOptions } from '@angular/http';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [TabsPage],
  providers:[ConfigProvider,I18nProvider,DatosProvider,Services,Http,RequestOptions ,ContactMeansProvider ]
})
export class TabsPageModule {}
