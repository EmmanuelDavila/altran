import { Component, OnInit } from "@angular/core";
import { ReCaptchaV3Service } from "ng-recaptcha";
import { HttpClient } from "@angular/common/http";
import {
  ToastController,
  NavController,
  LoadingController,
  AlertController,
} from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { Internaciolizacion } from "../modelo/Internacionalizacion";
import { ContactMeansProvider } from "../services/contact-means";
import { DatosProvider } from "../services/datos";
import { I18nProvider } from "../services/i18n";
import { Services } from "../services/services";
import { Idioma } from "../modelo/Idioma";
import { Pais } from "../modelo/Pais";


export let pepito;

@Component({
  selector: "app-tabs",
  templateUrl: "tabs.page.html",
  styleUrls: ["tabs.page.scss"],
})
export class TabsPage implements OnInit {
  tokenTabs: string;

  mySelectedIndex;
  pais: Pais = new Pais();
  idioma: Idioma = new Idioma();
  idiomas;
  internaciolizacion: Internaciolizacion = new Internaciolizacion();
  internaciolizaciones;

  primeraVez: boolean = false;
  comprobarVersion: boolean = false;
  isWhatshapp: boolean = false;
  isChat: boolean = false;

  version;
  localVersion;
  index;
  loading: any;

  titulo;
  message;
  negative;
  positive;
  token2;
  mostrar = false;
  country_code: any;
  codigo_compania: any;
  personalizacion: any;
  tokenOK: any;

  constructor(
    private recaptchaV3Service: ReCaptchaV3Service,
    public services: Services,
    public toastCtrl: ToastController,
    // public navParams: NavParams,
    public nav: NavController,
    public datos: DatosProvider,
    // public llamada: LlamadaProvider,
    public http: HttpClient,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    public i18n: I18nProvider,
    public contactMeans: ContactMeansProvider
  ) {}

  ngOnInit() {
    this.executeImportantAction();
    this.getDataPerson();
    this.datos.setPersonalizacion2().subscribe((res: any) => {
      console.log("setPersonalizacion2", res);
      this.personalizacion = res;
     // console.log('PRINCIPAL',this.services.tokenPrincipal);
      
    });
  }

  public executeImportantAction(): void {
    this.recaptchaV3Service
      .execute("testAccesoMAiAssist")
      .subscribe((token) => {
        console.info("TOKEN", token);
        this.tokenTabs = token;
       // this.getDatos(token);
        this.services.recaptcha(token).subscribe(
          (data: any) => {
            console.log("getDatos 3", data.token);

            pepito = data.token
          //  this.tokenOK = data.token
            console.log("data1", this.services.responseToken);
    
            this.datos.setRecaptcha(this.services.responseToken);
            Promise.all([
              this.services
                .updateCountries(this.country_code, this.codigo_compania)
                .subscribe(
                  (data:any) => {
                    console.log("data2", data);
    
                    this.pais.codigoPais = data.countries[0].countryCode;
                    this.pais.descripcion = data.countries[0].description;
                    this.pais.labelPais = data.countries[0].label;
                    this.pais.textoLegal = data.countries[0].legalText;
                    this.pais.tfnInter = data.countries[0].tfnInter;
                    this.pais.tfnLocal = data.countries[0].tfnLocal;
                    this.pais.url = data.countries[0].url;
                    this.datos.setPais(this.pais);
                  },
                  (err) => {}
                ),
              this.services
                .updateInternationalization(
                  this.country_code
                )
                .subscribe(
                  (data:any) => {
                    this.internaciolizaciones = [];
                    for (let i = 0; i < data.internationalization.length; i++) {
                      this.internaciolizacion = new Internaciolizacion();
                      this.internaciolizacion.idioma =
                        data.internationalization[i].id_language;
                      this.internaciolizacion.label =
                        data.internationalization[i].str_label;
                      this.internaciolizacion.traduccion =
                        data.internationalization[i].str_literal;
                      this.internaciolizaciones.push(this.internaciolizacion);
                    }
                    this.datos.setIntenacionalizacion(this.internaciolizaciones);
                    this.i18n.traducciones = this.datos.getInternacionalizacion();
                  },
                  (err) => {}
                ),
            ]).then(
              () => {
                this.mostrar = true;
                this.i18n.traducciones = this.datos.getInternacionalizacion();
                this.setContactMeans();
                //   this.isExternalCall();
                //this.loading.dismiss();
              },
              (err) => {
                this.mostrar = true;
                this.setContactMeans();
                //   this.isExternalCall();
                //this.loading.dismiss();
                this.translate.get("lenguaje_defecto").subscribe((res: string) => {
                  this.datos.lenguaje_defecto = res;
                });
    
                this.i18n.traducciones = this.datos.getInternacionalizacion();
              }
            );
          },
          (err) => {
            this.mostrar = true;
            //this.loading.dismiss();
            this.setContactMeans();
            // this.isExternalCall();
            this.translate.get("lenguaje_defecto").subscribe((res: string) => {
              this.datos.lenguaje_defecto = res;
            });
    
            this.i18n.traducciones = this.datos.getInternacionalizacion();
          }
        );
      });
  }

  getDataPerson() {
    this.http
      .get("../../assets/personalizacion/perso.json")
      .subscribe((res: any) => {
        console.log("DATA", res);

        (this.country_code = res.string.country_code),
          (this.codigo_compania = res.string.codigo_compania);
      });
  }

  // getDatos(token) {
  //   ////this.loading.dismiss();

  //   this.services.recaptcha(token).subscribe(
  //     (data: any) => {
  //       console.log("getDatos 3", data);

  //       console.log("data1", this.services.responseToken);

  //       this.datos.setRecaptcha(this.services.responseToken);
  //       Promise.all([
  //         this.services
  //           .updateCountries(this.country_code, this.codigo_compania)
  //           .subscribe(
  //             (data:any) => {
  //               console.log("data2", data);

  //               this.pais.codigoPais = data.countries[0].countryCode;
  //               this.pais.descripcion = data.countries[0].description;
  //               this.pais.labelPais = data.countries[0].label;
  //               this.pais.textoLegal = data.countries[0].legalText;
  //               this.pais.tfnInter = data.countries[0].tfnInter;
  //               this.pais.tfnLocal = data.countries[0].tfnLocal;
  //               this.pais.url = data.countries[0].url;
  //               this.datos.setPais(this.pais);
  //             },
  //             (err) => {}
  //           ),
  //         this.services
  //           .updateInternationalization(
  //             this.country_code
  //           )
  //           .subscribe(
  //             (data) => {
  //               this.internaciolizaciones = [];
  //               for (let i = 0; i < data.internationalization.length; i++) {
  //                 this.internaciolizacion = new Internaciolizacion();
  //                 this.internaciolizacion.idioma =
  //                   data.internationalization[i].id_language;
  //                 this.internaciolizacion.label =
  //                   data.internationalization[i].str_label;
  //                 this.internaciolizacion.traduccion =
  //                   data.internationalization[i].str_literal;
  //                 this.internaciolizaciones.push(this.internaciolizacion);
  //               }
  //               this.datos.setIntenacionalizacion(this.internaciolizaciones);
  //               this.i18n.traducciones = this.datos.getInternacionalizacion();
  //             },
  //             (err) => {}
  //           ),
  //       ]).then(
  //         () => {
  //           this.mostrar = true;
  //           this.i18n.traducciones = this.datos.getInternacionalizacion();
  //           this.setContactMeans();
  //           //   this.isExternalCall();
  //           //this.loading.dismiss();
  //         },
  //         (err) => {
  //           this.mostrar = true;
  //           this.setContactMeans();
  //           //   this.isExternalCall();
  //           //this.loading.dismiss();
  //           this.translate.get("lenguaje_defecto").subscribe((res: string) => {
  //             this.datos.lenguaje_defecto = res;
  //           });

  //           this.i18n.traducciones = this.datos.getInternacionalizacion();
  //         }
  //       );
  //     },
  //     (err) => {
  //       this.mostrar = true;
  //       //this.loading.dismiss();
  //       this.setContactMeans();
  //       // this.isExternalCall();
  //       this.translate.get("lenguaje_defecto").subscribe((res: string) => {
  //         this.datos.lenguaje_defecto = res;
  //       });

  //       this.i18n.traducciones = this.datos.getInternacionalizacion();
  //     }
  //   );
  // }

  setContactMeans() {
    this.isWhatshapp = this.contactMeans.getIsWhatsApp();
    this.isChat = this.contactMeans.getIsChat();

    return this.isWhatshapp && this.isChat;
  }
}
