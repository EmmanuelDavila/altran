import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { DatosProvider } from './services/datos';
import { Averia } from "./modelo/averia";
import { Idioma } from "./modelo/Idioma";
import { Internaciolizacion } from "./modelo/Internacionalizacion";
import { Llamar } from "./modelo/llamar";
import { Pais } from "./modelo/Pais";
import { Usuario } from "./modelo/usuario";
import { Vehiculo } from "./modelo/vehiculo";
import { Services } from './services/services';
import { Http } from "@angular/http";
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module, ReCaptchaV3Service } from "ng-recaptcha";
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    RecaptchaV3Module,
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Vehiculo,
    Usuario,
    DatosProvider,
    Averia,
    Pais,
    Services,
    Http,
    Internaciolizacion,
    Idioma,
    ReCaptchaV3Service,
    Llamar,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: "6LcGZYsUAAAAAHkhAGFoLWBLN8TPVaEnBanYhW70" }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
