import { Component, ViewChild } from '@angular/core';

import { NavController, Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from './tabs/tabs.page';
import { DatosProvider } from './services/datos';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild('mycontent') nav: NavController;
  rootPage: any = TabsPage;
  //menuOptions: MenuOption[];
  tabBarElement: any;

  constructor(
    public toastCtrl: ToastController,

    public platform: Platform,
    public translate: TranslateService,
   // private _menu: MenuController,
    private datos: DatosProvider
  ) {
    platform.ready().then(() => {

      this.configureBkBtnprocess();

      // Enable user notifications (will prompt the user to accept push notifications)
      this.registerServiceWorker();

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      translate.setDefaultLang('en');
      translate.use(navigator.language.substring(0, 2).toLowerCase() || 'en' );

      this.tabBarElement = document.querySelector('.tabbar.show-tabbar');


      this.requestNotificationPermission();
      // this.stepView(digitalData);
    });

  }

  // stepView(digitalData) {
  //   try {
  //     (window as any).stepView(digitalData);
  //   } catch (error) {
  //     console.warn('Error sending tag data:', error);
  //   }
  // }


  doOptionClicked(): void {
    //this.nav.setRoot(HomePage);
  }

  ngOnInit(): void {
    // Subscribe 
    this.subscribeUser();
  }

  // Request notification permission
  requestNotificationPermission(): void {
    if (Notification.requestPermission) {
      Notification.requestPermission(function (result) {
        console.log("NotificationPermission: ", result);

      });
    } else {
      console.log("Notificationssupported by this browser.");
    }
  }

  // Method that register the ServiceWorker 
  registerServiceWorker(): void {
    if ('service-worker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.register('service-worker.js').then(function (reg) {
          //console.log('Service Worker Registered!', reg);
          reg.pushManager.getSubscription().then(function (sub) {
            if (sub === null) {
              // Update UI to ask user to register for Push
              console.log('Not subscribed to push service!');
            } else {
              // We have a subscription, update the database
              console.log('Subscription object: ', sub);
            }
          });
        })
          .catch(function (err) {
            console.log('Service Worker registration failed: ', err);
          });
      });
    }
  }

  // Subscribe for notification web 
  subscribeUser(): void {
    if ('service-worker' in navigator) {
      window.addEventListener('load', () => {
        navigator.serviceWorker.ready.then(function (reg) {
          reg.pushManager.subscribe({
            userVisibleOnly: true
          }).then(function (sub) {
            console.log('Endpoint URL: ', sub.endpoint);
          }).catch(function (e) {
            if (Notification['permission'] === 'denied') {
              console.warn('Permission for notifications was denied');
            } else {
              console.error('Unable to subscribe to push', e);
            }
          });
        })
      })
    }
  }

  private configureBkBtnprocess() {

    // If you are on chrome (browser)
    if (window.location.protocol !== "file:") {

      // Register browser back button action and you can perform
      // your own actions like as follows
      window.onpopstate = (evt) => {

        // // Close menu if open
        // if (this._menu.isOpen()) {
        //   this._menu.close();
        //   return;
        // }

        // Close any active modals or overlays
        // let activePortal = this._ionicApp._loadingPortal.getActive() ||
        //   this._ionicApp._modalPortal.getActive() ||
        //   this._ionicApp._toastPortal.getActive() ||
        //   this._ionicApp._overlayPortal.getActive();

        // if (activePortal) {
        //   activePortal.dismiss();
        //   return;
        // }

        if (this.datos.pantalla == "asistencia") {
          //console.log(0);
          //this.platform.exitApp();
          return;
        } else if (this.datos.pantalla == "tabs") {
          //console.log(0.1)
          // this.platform.exitApp();
          return;
        }
        else if (this.datos.pantalla == "confirmacion") {
          // this.nav.setRoot(UbicacionTallerPage);
          //console.log(1);
          return;
        }
        else if (this.datos.pantalla == "datos") {
          // this.nav.setRoot(TabsPage);
          //console.log(2);
          return;
        }
        else if (this.datos.pantalla == "informacion") {
          // this.nav.setRoot(TabsPage);
          //console.log(3);
          return;
        }
        else if (this.datos.pantalla == "pasoFinal") { }
        //this.nav.setRoot(UbicacionTallerPage);
        else if (this.datos.pantalla == "reporte") {
          // this.nav.setRoot(DatosPersonalesPage);
          //console.log(4);
          return;
        }
        else if (this.datos.pantalla == "robo") {
          // this.nav.setRoot(TabsPage);
          //console.log(5);
          return;
        }
        else if (this.datos.pantalla == "accidente") {
          // this.nav.setRoot(TabsPage);
          //console.log(6);
          return;

        }
        else if (this.datos.pantalla == "ubicacion") {
          // this.nav.setRoot(ReportePage);
          //console.log(7);
          return;
        }
        else if (this.datos.pantalla == "ubicacionTaller") {
          // this.nav.setRoot(UbicacionPage);
          //console.log(8);
          return;
        }

      }

    } else {
      // you are in the app
    };

    // Fake browser history on each view enter
    // this.nav.subscribe((app) => {
    //   history.pushState(null, null, "");
    // });

  }




}
