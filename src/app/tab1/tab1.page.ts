import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";
import { LoadingController, NavController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { DatosProvider } from "../services/datos";

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"],
})
export class Tab1Page {
  public loading: any;
  hayDatos: boolean = false;
  vehiculos;
  nombre = "";
  hide_accident: Boolean = false;
  hide_robbery: Boolean = false;
  pais = "";

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public http: HttpClient,
    public datos: DatosProvider,
    public translate: TranslateService
  ) {
    this.datos.pantalla = "asistencia";
    this.getData();
  }

  getData() {
    this.http
      .get("../../assets/personalizacion/perso.json")
      .subscribe((res: any) => {
        console.log("DATA", res);

        this.hide_accident = res.bool.hide_accident;
        this.hide_robbery = res.bool.hide_robbery;
        this.pais = res.string.country_code;
      });
  }

  ionViewWillEnter() {
    this.datos.stepView("Asistencia");
  }

  goToReporte() {
    this.translate.get("lenguaje_defecto").subscribe((res: string) => {
      this.datos.lenguaje_defecto = res;
    });
    // this.navCtrl.setRoot(DatosPersonalesPage);
  }

  goToAccidente() {
    // this.navCtrl.push(AccidentePage);
  }

  goToLlamar() {
    // this.navCtrl.push(RoboPage);
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      spinner: "crescent",
    });

    this.loading.present();
  }
}
