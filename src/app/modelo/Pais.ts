import { Injectable } from '@angular/core';


@Injectable()
export class Pais {

    codigoPais: string;
    labelPais: string;
    textoLegal: string;
    descripcion: string;
    url: string;
    tfnLocal: string;
    tfnInter: string;

    constructor() {
    }

}