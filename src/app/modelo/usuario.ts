import { Injectable } from '@angular/core';


@Injectable()
export class Usuario {

    nombre: string;
    apellidos: string;
    telefono: string;
    email: string;
    direccion: string;
    ciudad: string;
    pais: string;
    clientCode: string;

    constructor() {
    }

}