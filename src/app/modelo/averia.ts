import { Injectable } from '@angular/core';


@Injectable()
export class Averia {
    address: string;
    locationType: string;
    longitude: string;
    city: string;
    province: string;
    zipCode: string;
    country: string;
    latitude: string;
    breakDownType: string;
    texto: string;
    longitudeTaller: string;
    latitudeTaller: string;
    hash: string;

    constructor() {
    }
}