import { Injectable } from '@angular/core';


@Injectable()
export class Vehiculo {

    bastidor: string;
    color: string;
    companyCode: string;
    marca: string;
    matricula: string;
    modelo: string;
    poliza: string;
    vehiculoCode: string;

    constructor() {
    }





}