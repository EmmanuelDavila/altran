import { Component, NgZone, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {
  ToastController,
  NavParams,
  AlertController,
  Platform,
  NavController,
} from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { Averia } from "../modelo/averia";
import { Usuario } from "../modelo/usuario";
import { Vehiculo } from "../modelo/vehiculo";
import { DatosProvider } from "../services/datos";
import { Services } from "../services/services";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: "app-tab2",
  templateUrl: "tab2.page.html",
  styleUrls: ["tab2.page.scss"],
})
export class Tab2Page {
  serverCode;
  usuario: Usuario = new Usuario();
  hayUser;
  form;
  form2: FormGroup;
  intentoGuardar: boolean = false;
  edicion: boolean;
  borrado: boolean;
  confirmarUbicacion: Boolean = false;
  averia = new Averia();
  idAveria;
  averiaLabel;
  vehiculo: Vehiculo = new Vehiculo();
  goToRoot: boolean = true;
  mensaje;
  hide_policy: Boolean = false;
  hide_matricula: Boolean = false;
  hide_bastidor: Boolean = false;
  matricula_y_bastidor_requeridos: Boolean = false;
  tabBarElement: any;
  public isShown: boolean = false; // YOU CAN INITIALIZE IN FALSE SO IT DOESN'T THROW ERROR AND BECAUSE IT'LL BE ON TOP OF PAGE.
  poliza_requerida: Boolean = true;

  constructor(
    public toastCtrl: ToastController,
  //  public services: Services,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    //public viewCtrl: ViewController,
    public datos: DatosProvider,
    public navCtrl: NavController,
    public http: HttpClient,
    public translate: TranslateService,
    public router: Router
  ) {
    // this.datos.stepView("Datos");
    this.datos.pantalla = "datos";

    this.usuario = this.datos.getUsuario();
    this.vehiculo = this.datos.getVehiculo();

    this.tabBarElement = document.querySelector(".tabbar.show-tabbar");
    this.http
      .get("../../assets/personalizacion/perso.json")
      .subscribe((res: any) => {
        console.log("DATA", res);

        this.hide_policy = res.bool.hide_policy;
        this.hide_matricula = res.bool.hide_matricula;
        this.hide_bastidor = res.bool.hide_bastidor
          ? res.bool.hide_bastidor
          : false;
        this.matricula_y_bastidor_requeridos =
          res.bool.matricula_y_bastidor_requeridos;
        this.poliza_requerida = res.bool.poliza_requerida;
        this.usuario.pais = res.string.country_code;
      });

    if (this.poliza_requerida != undefined) {
      this.poliza_requerida = this.poliza_requerida;
    }
    this.form = formBuilder.group({
      nombre: [
        this.usuario.nombre,
        Validators.compose([Validators.required, Validators.maxLength(40)]),
      ],
      apellidos: [this.usuario.apellidos, Validators.required],
      telefono: [
        this.usuario.telefono,
        Validators.compose([
          Validators.required,
          Validators.pattern("[0-9]*"),
          Validators.minLength(4),
          Validators.maxLength(12),
        ]),
      ],
      email: [this.usuario.email],
      //si queremos el email obligatorio, añadir: , Validators.compose([Validators.required, Validators.email])],
      pais: [this.usuario.pais],
    });

    this.form2 = formBuilder.group(
      {
        matricula: [this.vehiculo.matricula ? this.vehiculo.matricula : ""],
        marca: [this.vehiculo.marca, Validators.required],
        modelo: [this.vehiculo.modelo, Validators.required],
        color: [this.vehiculo.color, Validators.required],
        bastidor: [this.vehiculo.bastidor ? this.vehiculo.bastidor : ""],
        poliza: [this.vehiculo.poliza ? this.vehiculo.poliza : ""],
      },
      {
        validator: (formGroup: FormGroup) => {
          return this.campos(formGroup);
        },
      }
    );

    if (this.poliza_requerida) {
      this.form2.get("poliza").setValidators([Validators.required]);
    }
    if (this.matricula_y_bastidor_requeridos) {
      this.form2.get("matricula").setValidators([Validators.required]);
    } else {
      this.form2.get("matricula").setValidators([]);
    }

    if (this.matricula_y_bastidor_requeridos && !this.hide_bastidor) {
      this.form2.get("bastidor").setValidators([Validators.required]);
    } else {
      this.form2.get("bastidor").setValidators([]);
    }
  }

  getData() {
    this.http
      .get("../../assets/personalizacion/perso.json")
      .subscribe((res: any) => {
        console.log("DATA", res);

        this.hide_policy = res.personalizacion.bool.hide_policy;
        this.hide_matricula = res.personalizacion.bool.hide_matricula;
        this.hide_bastidor = res.personalizacion.bool.hide_bastidor
          ? res.personalizacion.bool.hide_bastidor
          : false;
        this.matricula_y_bastidor_requeridos =
          res.personalizacion.bool.matricula_y_bastidor_requeridos;
        this.poliza_requerida = res.personalizacion.bool.poliza_requerida;
        this.usuario.pais = res.personalizacion.string.country_code;
      });
  }

  campos(formGroup: FormGroup) {
    const hasMatricula: Boolean =
      formGroup.controls["matricula"] && formGroup.controls["matricula"].value;
    const hasBastidor: Boolean =
      formGroup.controls["bastidor"] && formGroup.controls["bastidor"].value;
    const hasPoliza: Boolean =
      formGroup.controls["poliza"] && formGroup.controls["poliza"].value;

    if (!this.matricula_y_bastidor_requeridos && !this.hide_bastidor) {
      if (hasMatricula || hasBastidor) {
        return null;
      } else {
        return { campos: true };
      }
    }

    if (!this.matricula_y_bastidor_requeridos && !this.poliza_requerida) {
      if (hasMatricula || hasPoliza) {
        return null;
      } else {
        return { campos: true };
      }
    }
  }

  goBack(): void {
    this.anterior();
  }

  ionViewWillEnter() {
    this.tabBarElement = document.querySelector(".tabbar.show-tabbar");

    // this.tabBarElement.style.display = 'none';
  }

  salvar() {}

  sendData() {}

  async presentToast(texto: string) {
    let toast = await this.toastCtrl.create({
      message: texto,
      duration: 3000,
      position: "bottom",
    });

    // toast.onDidDismiss(() => {
    //   console.log('Dismissed toast');
    // });

    toast.present();
  }
  async presentConfirm(mensaje) {
    let alert = await this.alertCtrl.create({
      message: mensaje,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.presentToast("cancelado");
          },
        },
        {
          text: "Ok",
          handler: () => {
            this.sendData();
          },
        },
      ],
    });
    alert.present();
  }

  checkErrors(): boolean {
    const numberPlate: string =
      this.form2.get("matricula") && this.form2.get("matricula").value;
    const policy: string =
      this.form2.get("poliza") && this.form2.get("poliza").value;
    const bastidor: string =
      this.form2.get("bastidor") && this.form2.get("bastidor").value;
    //FIXME: DESCOMENTAR
    if (this.hide_bastidor) {
      // MEXICO POR AHORA
      if (!numberPlate && !policy) {
        this.showToast();
        return false;
      } else {
        return true;
      }
    } else {
      if (!numberPlate && !bastidor) {
        this.showToast();
        return false;
      } else {
        return true;
      }
    }
  }

  showToast() {
    this.translate
      .get("datos_personales_toast_datos")
      .subscribe((res: string) => {
        this.mensaje = res;
      });
    this.presentToast(this.mensaje);
  }

  siguiente() {
    this.intentoGuardar = true;
    // if (this.checkErrors()) {
    // if (this.form.valid && this.form2.valid) {
    //   this.tabBarElement.style.display = "flex";
    //   this.goToRoot = false;
    //   this.datos.setUsuario(this.form);
    //   this.datos.setVehiculo(this.form2);
    //   this.router.navigate(['reporte-page']);
    // } else {
    //   this.showToast();
    // }
    this.datos.setUsuario(this.form);
    this.datos.setVehiculo(this.form2);
    this.router.navigate(['reporte']);
    // }
  }

  anterior() {
    this.tabBarElement.style.display = "flex";
    // this.navCtrl.setRoot(AsistenciaPage);
  }
}
