import { Injectable } from '@angular/core';
import { DatosProvider } from './datos';



@Injectable()
export class I18nProvider {

  //values in tabs.ts
  traducciones;

  constructor(public datos: DatosProvider) { }

  translate2(label, idioma){
    // console.log('label',label);
    // console.log('idioma',idioma);
    
    
  }

  translate(label, idioma) {
    for (var x = 0; x < this.traducciones.length; x++) {
      if (this.traducciones[x].label == label && this.traducciones[x].idioma == idioma) {
        return this.traducciones[x].traduccion;
      }
    }
    for (var y = 0; y < this.traducciones.length; y++) {
      if (this.traducciones[y].label == label && this.traducciones[y].idioma == 'EN' ) {
        return this.traducciones[y].traduccion;
      }
    }

    return "";
  }

}
