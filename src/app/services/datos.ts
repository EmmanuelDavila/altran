import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Averia } from "../modelo/averia";
import { Internaciolizacion } from "../modelo/Internacionalizacion";
import { Pais } from "../modelo/Pais";
import { Usuario } from "../modelo/usuario";
import { Vehiculo } from "../modelo/vehiculo";

declare var token;

@Injectable()
export class DatosProvider {
  personalizacion;
  //token;
  token2;
  //private usuario: Usuario = new Usuario();
  // private vehiculo: Vehiculo = new Vehiculo();
  // private averia: Averia = new Averia();
  lenguaje_defecto = "ES";
  pantalla;
  traducciones: Internaciolizacion[] = [];
  emailRexp = /^[_A-Za-z0-9-\+]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/i;

  digitalData = {
    screenName: "",
    section: "",
    subsection: "",
    section1: "",
    section2: "",
    section3: "",
    language: "",
    environment: "",
    userStatus: "",
    userDevice: "",
    screenSize: "",
    deviceOSVersion: "",
    deviceOS: "",
    appVersion: "",
    tipoApp: "PWA",
    pais: "",
    entidad: "",
  };

  private storage = window.localStorage;

  constructor(
    private usuario: Usuario,
    private vehiculo: Vehiculo,
    private averia: Averia,
    private internaciolizacion: Internaciolizacion,
    private pais: Pais,
    public http: HttpClient
  ) {
    //this.token = new Recaptchajs();
    this.http
      .get("../../assets/personalizacion/perso.json")
      .subscribe((res: any) => {
        console.log("DATA2", res);

        this.digitalData.pais = res.string.pais;
        this.digitalData.entidad = res.string.label_compania;
      });
  }

  stepView(screenName) {
    this.digitalData.screenName = screenName;
    this.digitalData.pais = this.digitalData.pais;
    this.digitalData.entidad = this.digitalData.entidad;
    try {
      (window as any).stepView(this.digitalData);
    } catch (error) {
      console.warn("Error sending tag data:", error);
    }
  }

  setPais(pais: Pais) {
    this.pais = new Pais();
    this.pais.codigoPais = pais.codigoPais;
    this.pais.labelPais = pais.labelPais;
    this.pais.textoLegal = pais.textoLegal;
    this.pais.descripcion = pais.descripcion;
    this.pais.url = pais.url;
    this.pais.tfnLocal = pais.tfnLocal;
    this.pais.tfnInter = pais.tfnInter;
  }

  getPais() {
    return this.pais;
  }

  setIntenacionalizacion(data: [Internaciolizacion]) {
    for (var i = 0; i < data.length; i++) {
      this.internaciolizacion = new Internaciolizacion();
      this.internaciolizacion.label = data[i].label;
      this.internaciolizacion.idioma = data[i].idioma;
      this.internaciolizacion.traduccion = data[i].traduccion;

      this.traducciones.push(this.internaciolizacion);
    }
  }

  getInternacionalizacion() {
    return this.traducciones;
  }

  setPersonalizacion(datos) {
    //console.log('setPersonalizacion', setPersonalizacion)
    this.http
    .get("../../assets/personalizacion/perso.json")
    .subscribe((res: any) => {
      console.log("DATA", res);

      this.personalizacion = res;
    });
    
  }

  setPersonalizacion2() {
    // console.log('setPersonalizacion', setPersonalizacion)
   return this.http.get("../../assets/personalizacion/perso.json")
    
  }

  getRecaptcha() {
    return this.token2;
  }

  setRecaptcha(recaptcha) {
    //console.log("prueba " + recaptcha)
    this.token2 = recaptcha;
  }

  setUsuario(usuario) {
    if (usuario.controls) {
      this.usuario.nombre = usuario.controls.nombre.value;
      this.usuario.apellidos = usuario.controls.apellidos.value;
      this.usuario.telefono = usuario.controls.telefono.value;
      if (usuario.controls.email.value) {
        if (this.emailValida(usuario.controls.email.value))
          this.usuario.email = usuario.controls.email.value;
        else this.usuario.email = "";
      }
      this.storage.setItem("user", JSON.stringify(this.usuario));
    } else this.storage.setItem("user", JSON.stringify(usuario));
  }

  /*Validacion mail */
  emailValida(email) {
    let expReg = this.emailRexp;

    if (expReg.test(email)) {
      return { emailValida: true };
    } else {
      return null;
    }
  }

  getUsuario() {
    if (this.storage.getItem("user") != undefined) {
      this.usuario = JSON.parse(this.storage.getItem("user"));
      return this.usuario;
    } else return this.usuario;
  }

  setVehiculo(vehiculo: any): any {
    if (vehiculo.controls) {
      this.vehiculo.marca = vehiculo.controls.marca.value;
      this.vehiculo.modelo = vehiculo.controls.modelo.value;
      this.vehiculo.color = vehiculo.controls.color.value;
      if (vehiculo.controls.poliza) {
        if (vehiculo.controls.poliza.value)
          this.vehiculo.poliza = vehiculo.controls.poliza.value;
        else this.vehiculo.poliza = "";
      } else this.vehiculo.poliza = "";
      if (vehiculo.controls.matricula) {
        if (vehiculo.controls.matricula.value)
          this.vehiculo.matricula = vehiculo.controls.matricula.value;
        else this.vehiculo.matricula = "";
      } else this.vehiculo.matricula = "";
      if (vehiculo.controls.bastidor) {
        if (vehiculo.controls.bastidor.value)
          this.vehiculo.bastidor = vehiculo.controls.bastidor.value;
        else this.vehiculo.bastidor = "";
      } else this.vehiculo.bastidor = "";
      this.storage.setItem("vehiculo", JSON.stringify(this.vehiculo));
    } else this.storage.setItem("vehiculo", JSON.stringify(vehiculo));
  }

  getVehiculo() {
    if (this.storage.getItem("vehiculo") != undefined) {
      this.vehiculo = JSON.parse(this.storage.getItem("vehiculo"));
      return this.vehiculo;
    } else return this.vehiculo;
  }

  setAveria(
    reporte: Averia,
    ubicacion: Averia,
    taller: Averia,
    confirmacion: Averia
  ) {
    if (reporte) {
      this.averia.breakDownType = reporte.breakDownType;
    }
    if (ubicacion) {
      this.averia.address = ubicacion.address;
      this.averia.city = ubicacion.city;
      this.averia.country = ubicacion.country;
      this.averia.locationType = ubicacion.locationType;
      this.averia.latitude = ubicacion.latitude;
      this.averia.longitude = ubicacion.longitude;
      this.averia.province = ubicacion.province;
      this.averia.zipCode = ubicacion.zipCode;
    }
    if (taller) {
      this.averia.latitudeTaller = taller.latitude;
      this.averia.longitudeTaller = taller.longitude;
    }
    if (confirmacion) {
      if (confirmacion.texto) this.averia.texto = confirmacion.texto;
      else this.averia.texto = "";
    }
  }

  getAveria() {
    return this.averia;
  }

  removeData() {
    this.usuario = new Usuario();
    this.vehiculo = new Vehiculo();
  }
}
