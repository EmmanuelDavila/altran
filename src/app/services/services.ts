import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/Rx";
import { ConfigProvider } from "./config-provider";
import { Usuario } from "../modelo/usuario";
import { Vehiculo } from "../modelo/vehiculo";
import { Averia } from "../modelo/averia";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatosProvider } from './datos';
import { RequestOptions } from "@angular/http";
import { pepito } from "../tabs/tabs.page";

/*
  Provider que maneja peticiones de empleados
*/
@Injectable()
export class Services {
  timeOut = 30000;
  responseToken: any;
  tokenPrincipal: any;

  constructor( public cli: HttpClient, public configHeaderProvider: ConfigProvider, private datos: DatosProvider) {}

  getVersion() {
    let resource = "maiassist?servicio=getVersion/";
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

    const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


    console.log('Authorization', headers);
    

    return  this.cli.get(options.url,  {headers})
  }

  updateCountries(codePais, compania) {
    let resource =
      "maiassist?servicio=updateCountries&pais=" +
      codePais +
      "&compania=" +
      compania;
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

    console.log('updateCountries',options.url);
    let a = this.cli.get(options.url).map((res:any)=> {
      console.log('updateCountries',res)
      let updateCountries = res;
      console.log('updateCountries', updateCountries);
      return updateCountries
      
     })



     console.log('updateCountriesPrincipal', a);

     const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


     console.log('Authorization', headers);
     

    return  this.cli.get(options.url,  {headers})
  }

  "Authorization"() {
    let resource = "maiassist?servicio=updateLanguages";
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

    const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


    console.log('Authorization', headers);
    
    return  this.cli.get(options.url,{headers} )
  }

  updateInternationalization(codePais) {
    let resource =
      "maiassist?servicio=updateInternationalization&pais=" + codePais;
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

    
    const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


    console.log('Authorization Internationalization', headers);

    return  this.cli.get(options.url, {headers})
  }



  getListOfBreakDownsByCountry(codePais, compania) {

    console.log('codePais',codePais)

    console.log('companiacompania',compania)

    let resource = 'maiassist?servicio=getListOfBreakDownsByCountry&country=' + codePais + '&company=' + compania;
    
    console.log('this.datos.getRecaptcha()', pepito)
    const  headers = new  HttpHeaders().set( "Authorization", `${pepito}`);

 let url = `https://sboo2dm1j5.execute-api.us-east-1.amazonaws.com/pre/maiassist?servicio=getListOfBreakDownsByCountry&country=${codePais}&company=${compania}`
    console.log('Authorization getListOfBreakDownsByCountry', {headers});

    return  this.cli.get(url, {headers})

}

  updateClientData(
    usuario: Usuario,
    serverCode: string,
    deviceProcedence: string,
    codigoCompania: string,
    promo
  ) {
    //: los códigos normalizados para deviceProcedence son los siguientes
    // 0: iPhone.
    // 1: Android.
    // 2: BlackBerry.
    //En el caso de un alta nueva de usuario, el campo “serverCode” se envía vacío.
    //De esa forma el servidor interpreta que se trata de un alta. Si se envía un serverCode, el servidor interpreta que se trata de una actualización.

    let ciudad;
    if (usuario.ciudad == "") ciudad = ".";
    else ciudad = usuario.ciudad;

    if (usuario.email == null || !usuario.email) usuario.email = "";

    let resource =
      "maiassist?servicio=updateClientData&serverCode=" +
      serverCode +
      "&personalDataAddress=" +
      usuario.direccion +
      "&personalDataCountry=" +
      usuario.pais +
      "&personalDataLastName=" +
      usuario.apellidos +
      "&personalRDGPPromo=" +
      promo +
      "&personalDataPhone=" +
      usuario.telefono +
      "&personalDataEmail=" +
      usuario.email +
      "&personalDataCity=" +
      ciudad +
      "&companyCode=" +
      codigoCompania +
      "&deviceProcedence=" +
      deviceProcedence +
      "&personalRDGPCheck=1&personalDataName=" +
      usuario.nombre +
      "&personalRDGPVersion=437&";
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

 
    const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


    console.log('Authorization Internationalization', headers);

    return  this.cli.get(options.url, {headers})
  }

  updateVehicleData(
    vehiculo: Vehiculo,
    serverCode: string,
    borrado: boolean,
    clientServerCode: string,
    companyCode: string,
    company: string
  ) {
    //clientServerCode 	 cadena que contiene el identificador de BBDD del usuario

    //En el caso de un alta nueva de vehículo, el campo “serverCode” se envía vacío. De esa forma el servidor interpreta que se trata de un alta.
    //Si se envía un serverCode, y borrado vale “false”, el servidor interpreta que se trata de una actualización.
    //Si se envía un serverCode y borrado vale “true” el servidor interpreta que se trata de una eliminación.

    let resource =
      "maiassist?servicio=updateVehicleData&companyCode=" +
      companyCode +
      "&serverCode=" +
      serverCode +
      "&borrado=" +
      borrado +
      "&poliza=" +
      vehiculo.poliza.toUpperCase() +
      "&matricula=" +
      vehiculo.matricula.toUpperCase() +
      "&color=" +
      vehiculo.color +
      "&clientServerCode=" +
      clientServerCode +
      "&bastidor=" +
      vehiculo.bastidor.toUpperCase() +
      "&company=" +
      company +
      "&modelo=" +
      vehiculo.modelo +
      "&marca=" +
      vehiculo.marca +
      "&/";
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

 
    const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


    console.log('Authorization Internationalization', headers);

    return  this.cli.get(options.url, {headers})
  }

  getInfoForAssistance(idSolicitud) {
    let resource = "getInfoForAssistance?idSolicitud=" + idSolicitud;
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

    return this.cli.request(options.body, options.url).subscribe((res: any) => {
      return res.json();
    });
  }

  createAssistanceRequest(
    averia: Averia,
    clientCode: string,
    vehicleCode: string,
    pais,
    telefono
  ) {
    // locationType 	    cadena que indica si la localización facilitada por el usuario es exacta (“0” exacta, “1” no exacta)
    //En el caso de un alta nueva de vehículo, el campo “serverCode” se envía vacío. De esa forma el servidor interpreta que se trata de un alta.
    //Si se envía un serverCode, y borrado vale “false”, el servidor interpreta que se trata de una actualización.
    //Si se envía un serverCode y borrado vale “true” el servidor interpreta que se trata de una eliminación.

    let resource =
      "createAssistanceRequest?address=" +
      averia.address +
      "&requestType=2&locationType=" +
      averia.locationType +
      "&longitude=" +
      averia.longitude +
      "&city=" +
      averia.city +
      "&state=" +
      averia.province +
      "&vehicleCode=" +
      vehicleCode +
      "&clientCode=" +
      clientCode +
      "&zipCode=" +
      averia.zipCode +
      "&country=" +
      averia.country +
      "&latitude=" +
      averia.latitude +
      "&breakdownType=" +
      averia.breakDownType +
      "&freeText=" +
      averia.texto +
      "&destinationLongitude=" +
      averia.longitudeTaller +
      "&destinationLatitude=" +
      averia.latitudeTaller +
      "&personalDataCountry=" +
      pais +
      "&personalDataPhone=" +
      telefono;
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

    const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


    console.log('Authorization Internationalization', headers);

    return  this.cli.get(options.url, {headers})
  }

  getGeolocalizacion(lat, lng, pais) {
    let resource =
      "geocode?latlng=" + lat + "," + lng + "&sensor=true&language=" + pais;
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

    return this.cli.request(options.body, options.url).subscribe((res: any) => {
      return res.json();
    });
  }

  getGeolocalizacionAddress(address, pais) {
    let resource =
      "geocode?address=" + address + "&sensor=true&language=" + pais;
    let options: RequestOptions = this.configHeaderProvider.config(
      "GET",
      resource
    );

 
    const  headers = new  HttpHeaders().set( "Authorization", `${this.responseToken}`);


    console.log('Authorization Internationalization', headers);

    return  this.cli.get(options.url, {headers})
  }

  recaptcha(token) {

    console.log('recaptcha(token)', token);

    let resource = "recaptcha?token=" + token;
    let options: RequestOptions = this.configHeaderProvider.configRecaptcha(
      "GET",
      resource
    );

   console.log(options.url);
     this.cli.get(options.url).map((res:any)=> {
      console.log('rr',res)
      let rr = res;
      console.log('holña', rr.token);
      this.tokenPrincipal = rr;      
     })

    return  this.cli.get(options.url)
  }
}
