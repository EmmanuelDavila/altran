import { Injectable } from '@angular/core';
import { RequestMethod, Headers, RequestOptions } from '@angular/http';
import { DatosProvider } from './datos';
import { HttpClient } from '@angular/common/http';


/*
  Provider para el manejo de la configuracion de la cabecera para las peticiones
*/

declare var token;

@Injectable()
export class ConfigProvider {
    //public APIREST_URL_OLD: string = 'https://2si6ssdnp8.execute-api.eu-west-1.amazonaws.com/pre/';
    // public APIREST_URL: string = 'https://veffmf98q9.execute-api.us-east-1.amazonaws.com/pre/';
    public APIREST_URL: string = 'https://sboo2dm1j5.execute-api.us-east-1.amazonaws.com/pre/';
    environment: any;

    constructor(public datos: DatosProvider,private http: HttpClient) {
    }

    /**
    * Configuracion peticion
    * @param Method 'GET' o 'POST' o 'PUT' o 'DELETE'
    * @param URL
    * @param Body
    */

    config(Method: string, URL: string, Body?: any): RequestOptions {
        this.http
        .get("../../assets/personalizacion/perso.json")
        .subscribe((res: any) => {
          console.log("DATA", res);
  
          this.environment = res.string.environment;
        });
        let metodo: number;
        Method.toUpperCase() == 'GET' ? metodo = RequestMethod.Get : null;
        Method.toUpperCase() == 'POST' ? metodo = RequestMethod.Post : null;
        Method.toUpperCase() == 'PUT' ? metodo = RequestMethod.Put : null;
        Method.toUpperCase() == 'DELETE' ? metodo = RequestMethod.Delete : null;

        let headers = new Headers({
            "Authorization": this.datos.getRecaptcha()
        });

        let options = new RequestOptions({ headers: headers });
        if (this.environment)
            options.url = this.APIREST_URL.concat(URL + "&env=EUR");
        else
            options.url = this.APIREST_URL.concat(URL);
        options.withCredentials = false;
        options.method = metodo;

        Body ? options.body = JSON.stringify(Body) : null;
        return options;
    }

    configRecaptcha(Method: string, URL: string, Body?: any): RequestOptions {
        let metodo: number;
        Method.toUpperCase() == 'GET' ? metodo = RequestMethod.Get : null;
        Method.toUpperCase() == 'POST' ? metodo = RequestMethod.Post : null;
        Method.toUpperCase() == 'PUT' ? metodo = RequestMethod.Put : null;
        Method.toUpperCase() == 'DELETE' ? metodo = RequestMethod.Delete : null;

        let headers = new Headers({
            'Accept': 'application/json, text/javascript, */*; q=0.01'
        });
        let options = new RequestOptions({ headers: headers });
        options.url = this.APIREST_URL.concat(URL);
        options.withCredentials = false;
        options.method = metodo;

        Body ? options.body = JSON.stringify(Body) : null;
        return options;
    }


}
