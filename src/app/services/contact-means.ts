import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import { DatosProvider } from "./datos";
import { TranslateService, LangChangeEvent } from "@ngx-translate/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ContactMeansProvider {
  public static URL_WHATSHAPP = "https://api.whatsapp.com/send";
  public static URL_CHAT =
    "https://c4m-pre.ma-myassistance.com/4M/client/directChat.jsp";
  private whatsAppData: any;
  private chatInfo: any;
  private isWhatshApp: boolean;
  private isChat: boolean;
  coutry_code: any;
  nombreApp: any;

  constructor(
    public datos: DatosProvider,
    public translate: TranslateService,
    public http: HttpClient
  ) {
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.setContactMeansData();
    });
  }

  getData() {
    this.http
      .get("../../assets/personalizacion/perso.json")
      .subscribe((res: any) => {
        console.log("DATA", res);
        this.isWhatshApp = res.bool.is_whatshapp;
        this.isChat = res.bool.is_chat;
        this.coutry_code= res.string.coutry_code;
        this.nombreApp = res.string.nombreApp
      });
  }

  public setContactMeansData() {
    const msg = this.translate.instant("whatsapp_msg"),
      phone = this.translate.instant("telefono_whatsapp");

    this.http.get("assets/personalizacion/perso.json").subscribe((data) => {
      this.isWhatshApp;
      this.isChat;
      const whatshAppMsg = encodeURIComponent(msg.trim());
      this.whatsAppData = {
        text: whatshAppMsg,
        phone: phone,
      };

      this.chatInfo = {
        chatToken: "A" + this.coutry_code,
        nameApp: "(" + this.nombreApp + ")",
        chatChannel: "CHATPWA" + this.coutry_code,
      };
    });
  }

  getWhatsAppUrl(): string {
    return (
      ContactMeansProvider.URL_WHATSHAPP +
      "?phone=" +
      this.whatsAppData.phone +
      "&text=" +
      this.whatsAppData.text
    );
  }

  getChatUrl() {
    let clientName = this.chatInfo.nameApp;
    const userData = this.datos.getUsuario();

    if (Object.keys(userData).length > 0) {
      clientName =
        userData.apellidos + ", " + userData.nombre + " " + clientName;
    }
    return (
      ContactMeansProvider.URL_CHAT +
      "?chatToken=" +
      this.chatInfo.chatToken +
      "&clientName=" +
      clientName +
      "&chatChannel=" +
      this.chatInfo.chatChannel +
      "&fontSize=16"
    );
  }

  getIsWhatsApp() {
    return this.isWhatshApp;
  }

  getIsChat() {
    return this.isChat;
  }
}
